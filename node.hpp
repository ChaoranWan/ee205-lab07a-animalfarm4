///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo Andrew <@todo chaoran@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02-02-2001
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <random>
#include <array>
#include <list>

#pragma once

class Node {
/*   public:
      const bool empty() const;
      void push_front( Node* newNode );
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      unsigned int size() const;*/
   protected:
      Node* next = nullptr;
/*      const bool empty() const;
      void push_front( Node* newNode );
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      unsigned int size() const;
*/
   friend class SingleLinkedList;
};
