///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>
#include <random>
#include "node.hpp"

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN };  /// @todo Add more colors

//static const Gender getRandomGender();

class Animal : public Node {
public:

   Animal();
   ~Animal();


	enum Gender gender;
	string      species;

	virtual const string speak() = 0;
	
   static const Gender getRandomGender();
   static const Color getRandomColor();
   static const bool getRandomBool();
   static const float getRandomWeight(const float from, const float to);
   static const string getRandomName();
  

	void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender); 
};

class AnimalFactory {
   public:
      static Animal* getRandomAnimal();

};} // namespace animalfarm

