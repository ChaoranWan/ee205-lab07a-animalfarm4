///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo Andrew <@todo chaoran@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02-02-2001
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <random>
#include <array>
#include <list>

#include "node.hpp"

/*
const bool Node::empty() const { 
   return next == nullptr;
}

void Node::push_front(Node* newNode) {
   next = newNode;
}
*/
