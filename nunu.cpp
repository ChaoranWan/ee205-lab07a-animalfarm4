///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author @todo Andrew <@todo chaoran@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02-02-2001
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool isNative, enum Color newColor, enum Gender newGender ) {
   switch (isNative) {
      case false: native = "false"; break;
      case true: native = "true"; break;
//      case false: native = "false"; break;
   }                                
//   native = isNative;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship
   gender = newGender;
   species = "Fistularia chinensis";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   scaleColor = newColor;       /// A has-a relationship, so it comes through the constructor
   favoriteTemp = 80.6;       /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
   //name /// A has-a relationship.  Every cat has its own name.
}


//const string Fish::speak() {
//   return string( "Bubble Bubble" );
//}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << "   Is native = [" << native << "]" << endl;
   Fish::printInfo();
}

}
