///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <random>
#include <cstdlib>
#include <ctime>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;

namespace animalfarm {

Animal::Animal() {
   cout << ".";
}

Animal::~Animal() {
   cout << "x";
}

const Gender Animal::getRandomGender() {
   int i;
   static random_device rd;
   static std::mt19937 gen(time(nullptr));
   static std::uniform_int_distribution<> dis(0, 1);
   i = dis(gen);
   switch (i) {
      case 0: return MALE;
      case 1: return FEMALE;
//      case 2: return UNKNOWN;
   }
   return UNKNOWN;
};

const Color Animal::getRandomColor () {
   int i;
   static random_device rd;
   static std::mt19937 gen(time(nullptr));
   static std::uniform_int_distribution<> dis(0, 5);
   i = dis(gen);
   switch (i) {
      case 0: return BLACK;
      case 1: return WHITE;
      case 2: return RED;
      case 3: return SILVER;
      case 4: return YELLOW;
      case 5: return BROWN;
   }
   return BLACK; //default if none of the other cases
};

const bool Animal::getRandomBool() {
   int i;
   static random_device rd;
   static std::mt19937 gen(time(nullptr));
   static std::uniform_int_distribution<> dis(0, 1);
   i = dis(gen);
   switch (i) {
      case 0: return true;
      case 1: return false;
   }
   return true; //defaults to true

};

const float Animal::getRandomWeight(const float from, const float to) {
   srand (static_cast <unsigned> (time(0)));
   float rando;
   rando = from + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(to-from)));
//   cout << rando << endl;
   return rando;
}

const string Animal::getRandomName() {
   string str;
   int leng;
   int rando;
   char ch;
   static random_device rd;
   static std::mt19937 gen(time(nullptr));
   static std::uniform_int_distribution<> dis(0, 5);
   rando = dis(gen);
   leng = rando + 4;
//   cout << leng << endl;
   static std::uniform_int_distribution<> des(0, 23);
   rando = des(gen);
   ch = 'a';
   ch = ch + rando;
   str += toupper(ch);
   for (int i = 1; i < leng; i++) {
      rando = des(gen);
      ch = 'a';
      ch = ch + rando;
      str += ch;
   }
//   cout << str << endl;
   return str;
}



void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
	switch (color) {
      case BLACK: return string("Black");
      case WHITE: return string("White");
      case RED: return string("Red");
      case SILVER: return string("Silver");
      case YELLOW: return string("Yellow");
      case BROWN: return string("Brown");
   }
	///       on Animal Farm 1
   return string("Unknown");
};

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
   int i;
   static random_device rd;
   static std::mt19937 gen(time(nullptr));
   static std::uniform_int_distribution<> dis(0, 5);
   i = dis(gen);
// i = 3;
      switch (i) {
         case 0: newAnimal = new Cat ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 1: newAnimal = new Dog ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 2: newAnimal = new Nunu ( Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 3: newAnimal = new Aku ( Animal::getRandomWeight(10.5, 55.6), Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 4: newAnimal = new Palila( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 5: newAnimal = new Nene ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      }
   return newAnimal;
};
}
//D namespace animalfarm
