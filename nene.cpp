///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author @todo Andrew <@todo chaoran@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02-02-2001
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string newId, enum Color newColor, enum Gender newGender ) {
   id = newId;
   isMigratory = "true";
   gender = newGender;
   species = "Branta sandvicensis";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
}


const string Nene::speak() {
   return string( "Nay, Nay" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << id << "]" << endl;
   Bird::printInfo();
}

}
