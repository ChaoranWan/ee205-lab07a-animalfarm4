///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo Andrew <@todo chaoran@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02-02-2001
///////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include "list.hpp"

const bool SingleLinkedList::empty() const {
   return head == nullptr;
}

void SingleLinkedList::push_front(Node* newNode) {
   if (newNode == nullptr) {
      return;
   }
   newNode->next = head;
   head = newNode;
}

Node* SingleLinkedList::get_first() const {
   return head;
}

Node* SingleLinkedList::pop_front() {
    if (head == nullptr)
        return nullptr;
    Node* newHead = head;
    head = head->next;
//    delete newHead;
    return newHead;

//   return nullptr;
}

Node* SingleLinkedList::get_next( const Node* currentNode ) const {
/*   Node* nextNode = &currentNode;
   nextNode = nextNode->next;
   return nextNode;
*/
   return currentNode->next;
}

unsigned int SingleLinkedList::size() const {
		Node* temp = head;
		int count = 0;
		while (temp != nullptr) {
			count++;
			temp = temp->next;
		}
		return count;
}

